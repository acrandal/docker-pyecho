# Docker PyEcho

Simple python TCP echo server in a docker image.

Just designed to implement a simple network service using Docker.
Nothing too fancy, it just echos your TCP strings.
The Makefile will set it up to listen publicly on port 8022.