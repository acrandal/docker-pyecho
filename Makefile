# Simple makeifle to document commands for build, publish, and run
#  Aaron S. Crandall <acrandal@gmail.com> 2020
#
# Publish won't work unless you're me: acrandal
# If you login to DockerHub with a different ID, chnage out 'acrandal' for it
# Also, this expects you to have your own 'pyecho' project on DockerHub
# Basically, see the Docker intro tutorial here:
#  https://docs.docker.com/get-started/
#


build:
	@echo "Building docker image"
	docker build --tag pyecho:latest .

rebuild:
	@echo "Rebuilding without cache to force fresh image contents"
	docker build --no-cache --tag pyecho:latest .

publish: build
	@echo "Publishing image to dockerhub"
	docker tag pyecho:latest acrandal/pyecho:latest
	docker push acrandal/pyecho:latest

run:
	@echo "Running pyecho with defaults on port 8022"
	docker run --publish 8022:1337 -d pyecho:latest

login:
	@echo "Logging in this account to DockerHub"
	docker login
