FROM alpine:latest

WORKDIR /opt
COPY server.py .
RUN apk add python3

EXPOSE 1337
CMD [ "python3", "/opt/server.py" ]

#COPY . .
